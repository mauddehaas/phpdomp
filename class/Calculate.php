<?php


class Calculate
{

    public function calculateNumber($number1, $number2 , $var)
    {
        if($var == '+')
        {
            $outcome = $number1 + $number2 ;
        }
        elseif($var == '-')
        {
            $outcome = $number1 - $number2 ;
        }
        elseif($var == '/')
        {
            $outcome = $number1 / $number2 ;
        }
        elseif($var == '%')
        {
            $outcome = $number1 % $number2 ;
        }
        else
            {
                $outcome = "alleen +, *, / en % zijn toegestaan";
            }

        return $outcome;
    }

}